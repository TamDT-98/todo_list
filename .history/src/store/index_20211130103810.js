import { createStore } from "vuex";

import axios from "axios";

// const defaultListTasks = [
// {
//   task: "Play soccer",
//   date: "2021-12-17",
//   time: "11:58",
//   completed: false,
//   edit: false,
// },
// {
//   task: "Go back home",
//   date: "2021-11-23",
//   time: "11:58",
//   completed: false,
//   edit: false,
// },
// {
//   task: "Learn Javascript",
//   date: "2021-11-14",
//   time: "11:58",
//   completed: false,
//   edit: false,
// },
// {
//   task: "Buy something",
//   date: "2021-12-25",
//   time: "11:58",
//   completed: false,
//   edit: false,
// },
// {
//   task: "Do something with ReactJS",
//   date: "2021-12-26",
//   time: "11:58",
//   completed: false,
//   edit: false,
// },
// {
//   task: "Do something with JavaScript",
//   date: "2021-11-24",
//   time: "11:58",
//   completed: false,
//   edit: false,
// },
// ];

export default createStore({
  state: {
    defaultListTasks: [],
    listTasks: [],
  },
  getters: {
    listTasks: (state) => state.listTasks,
  },
  mutations: {
    addTasks(state, task) {
      state.listTasks.unshift(task);
    },

    removeTasks(state, id) {
      state.listTasks.splice(id, 1);
    },

    filterTasks(state, search) {
      if (search.length > 0) {
        state.listTasks = state.defaultListTasks.filter((taskFilter) => {
          return taskFilter.task.toLowerCase().includes(search.trim());
        });
      } else state.listTasks = state.defaultListTasks;
      return state.listTasks;
    },

    sortListTasks(state, selected) {
      if (selected === "Default") {
        return (state.listTasks = state.defaultListTasks.sort(
          (a, b) => new Date(a.date) - new Date(b.date)
        ));
      } else if (selected === "A - Z") {
        return (state.listTasks = state.defaultListTasks.sort((a, b) => {
          if (a.task.toUpperCase() < b.task.toUpperCase()) {
            return -1;
          } else if (a.task.toUpperCase() > b.task.toUpperCase()) {
            return 1;
          } else return 0;
        }));
      } else if (selected === "Z - A") {
        return (state.listTasks = state.defaultListTasks.sort((a, b) => {
          if (a.task.toUpperCase() < b.task.toUpperCase()) {
            return 1;
          } else if (a.task.toUpperCase() > b.task.toUpperCase()) {
            return -1;
          } else return 0;
        }));
      } else if (selected === "Completed") {
        return (state.listTasks = state.defaultListTasks.filter(
          (completedFilter) => {
            return completedFilter.completed === true;
          }
        ));
      } else if (selected === "Unfinished") {
        return (state.listTasks = state.defaultListTasks.filter(
          (completedFilter) => {
            return completedFilter.completed === false;
          }
        ));
      }
    },
  },
  actions: {
    getListTasks({ commit }) {
      axios
        .get("http://localhost:3000/defaultListTasks")
        .then(state.defaultListTasks = response.data).catch((error) => {
          console.log(error);
        });
        }
        
    },
  },
  modules: {},
});
