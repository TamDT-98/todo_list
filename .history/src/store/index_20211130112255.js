import { createStore } from "vuex";

import * as actions from "./actions";

const defaultListTasks = [];

export default createStore({
  state: {
    // defaultListTasks: [],
    listTasks: defaultListTasks,
  },
  getters: {
    listTasks: (state) => state.listTasks,
  },
  mutations: {
    addTasks(state, task) {
      state.listTasks.unshift(task);
    },

    removeTasks(state, id) {
      state.listTasks.splice(id, 1);
    },

    filterTasks(state, search) {
      if (search.length > 0) {
        state.listTasks = state.defaultListTasks.filter((taskFilter) => {
          return taskFilter.task.toLowerCase().includes(search.trim());
        });
      } else state.listTasks = state.defaultListTasks;
      return state.listTasks;
    },

    sortListTasks(state, selected) {
      if (selected === "Default") {
        return (state.listTasks = state.defaultListTasks.sort(
          (a, b) => new Date(a.date) - new Date(b.date)
        ));
      } else if (selected === "A - Z") {
        return (state.listTasks = state.defaultListTasks.sort((a, b) => {
          if (a.task.toUpperCase() < b.task.toUpperCase()) {
            return -1;
          } else if (a.task.toUpperCase() > b.task.toUpperCase()) {
            return 1;
          } else return 0;
        }));
      } else if (selected === "Z - A") {
        return (state.listTasks = state.defaultListTasks.sort((a, b) => {
          if (a.task.toUpperCase() < b.task.toUpperCase()) {
            return 1;
          } else if (a.task.toUpperCase() > b.task.toUpperCase()) {
            return -1;
          } else return 0;
        }));
      } else if (selected === "Completed") {
        return (state.listTasks = state.defaultListTasks.filter(
          (completedFilter) => {
            return completedFilter.completed === true;
          }
        ));
      } else if (selected === "Unfinished") {
        return (state.listTasks = state.defaultListTasks.filter(
          (completedFilter) => {
            return completedFilter.completed === false;
          }
        ));
      }
    },

    GET_LIST_TASKS(defaultListTasks, getListTasks) {
      this.defaultListTasks = getListTasks;
    },
  },
  actions,
  modules: {},
});
