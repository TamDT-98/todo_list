import Vuex from "vuex";

Vuex.use(Vuex);

const store = new Vuex.store({
  state: {
    listTasks: [
      {
        task: "Learn VueJS",
        date: "17/11/2021",
        time: "11:58AM",
        completed: false,
      },
      {
        task: "Learn React",
        date: "17/11/2021",
        time: "11:58AM",
        completed: false,
      },
      {
        task: "Learn Javascript",
        date: "17/11/2021",
        time: "11:58AM",
        completed: false,
      },
      {
        task: "Do something with VueJS",
        date: "17/11/2021",
        time: "11:58AM",
        completed: false,
      },
    ],
  },
  getters: {
    listTasks: (state) => state.listTasks,
  },
  mutations: {},
  actions: {},
  modules: {},
});

export default storel;
