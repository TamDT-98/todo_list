import { createStore } from "vuex";

import * as actions from "./actions";
import axios from "axios";

export default createStore({
  state: {
    defaultListTasks: [],
    listTasks: [],
  },
  getters: {
    listTasks: (state) => state.listTasks,
  },
  mutations: {
    filterTasks(state, search) {
      if (search.length > 0) {
        state.listTasks = state.defaultListTasks.filter((taskFilter) => {
          return taskFilter.task.toLowerCase().includes(search.trim());
        });
      } else state.listTasks = state.defaultListTasks;
      return state.listTasks;
    },

    sortListTasks(state, selected) {
      console.log("selected 3: ", selected);
      if (selected === "Default") {
        return state.defaultListTasks.sort(
          (a, b) => new Date(a.date) - new Date(b.date)
        );
      } else if (selected === "A - Z") {
        return state.defaultListTasks.sort((a, b) => {
          if (a.task.toUpperCase() < b.task.toUpperCase()) {
            return -1;
          } else if (a.task.toUpperCase() > b.task.toUpperCase()) {
            return 1;
          } else return 0;
        });
      } else if (selected === "Z - A") {
        return state.defaultListTasks.sort((a, b) => {
          if (a.task.toUpperCase() < b.task.toUpperCase()) {
            return 1;
          } else if (a.task.toUpperCase() > b.task.toUpperCase()) {
            return -1;
          } else return 0;
        });
      } else if (selected === "Completed") {
        return state.defaultListTasks.filter((completedFilter) => {
          return completedFilter.completed === true;
        });
      } else if (selected === "Unfinished") {
        return state.defaultListTasks.filter((completedFilter) => {
          return completedFilter.completed === false;
        });
      }
    },

    GET_LIST_TASKS(state, getListTasks) {
      state.defaultListTasks = getListTasks;
      state.listTasks = state.defaultListTasks;
    },
  },
  actions,
  modules: {},
});
