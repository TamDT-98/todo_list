import { createStore } from "vuex";

import * as actions from "./actions";

export default createStore({
    state: {
        listTasks: [],
    },
    getters: {
        listTasks: (state) => state.listTasks,
    },
    mutations: {
        filterTasks(state, search) {
            console.log("search: ", search);
            if (search.length > 0) {
                state.listTasks.filter((taskFilter) => {
                    console.log("taskFilter: ", taskFilter.task);
                    console.log("search: ", search);
                    return taskFilter.task
                        .toLowerCase()
                        .includes(search.trim().toLowerCase());
                });
            }
        },

        sortListTasks(state, selected) {
            if (selected === "Default") {
                return state.listTasks.sort(
                    (a, b) => new Date(b.date) - new Date(a.date)
                );
            } else if (selected === "A - Z") {
                return state.listTasks.sort((a, b) => {
                    if (a.task.toUpperCase() < b.task.toUpperCase()) {
                        return -1;
                    } else if (a.task.toUpperCase() > b.task.toUpperCase()) {
                        return 1;
                    } else return 0;
                });
            } else if (selected === "Z - A") {
                return state.listTasks.sort((a, b) => {
                    if (a.task.toUpperCase() < b.task.toUpperCase()) {
                        return 1;
                    } else if (a.task.toUpperCase() > b.task.toUpperCase()) {
                        return -1;
                    } else return 0;
                });
            } else if (selected === "Completed") {
                return state.listTasks.filter((completedFilter) => {
                    return completedFilter.completed === true;
                });
            } else if (selected === "Unfinished") {
                return state.listTasks.filter((completedFilter) => {
                    return completedFilter.completed === false;
                });
            }
        },

        GET_LIST_TASKS(state, getListTasks) {
            state.listTasks = getListTasks.sort(
                (a, b) => new Date(b.date) - new Date(a.date)
            );
        },

        ADD_TASK(state, tasks) {
            state.listTasks.unshift(tasks);
        },

        UPDATE_TASK(state, tasks) {
            console.log(state.listTasks);
            state.listTasks[tasks.id] = tasks;
            console.log("state.listTasks: ", state.listTasks);
        },

        GET_ID_TASK(state, id) {
            state.listTasks = [id];
        },

        REMOVE_TASK(state, task) {
            return state.listTasks.splice(task, 1);
        },
    },
    actions,
    modules: {},
});