import { createStore } from "vuex";

const defaultListTasks = [
  {
    task: "Play soccer",
    date: "2021-12-17",
    time: "11:58",
    completed: false,
    edit: false,
  },
  {
    task: "Go back home",
    date: "2021-11-23",
    time: "11:58",
    completed: false,
    edit: false,
  },
  {
    task: "Learn Javascript",
    date: "2021-11-14",
    time: "11:58",
    completed: false,
    edit: false,
  },
  {
    task: "Do something with VueJS",
    date: "2021-12-25",
    time: "11:58",
    completed: false,
    edit: false,
  },
  {
    task: "Do something with ReactJS",
    date: "2021-12-26",
    time: "11:58",
    completed: false,
    edit: false,
  },
  {
    task: "Do something with JavaScript",
    date: "2021-11-24",
    time: "11:58",
    completed: false,
    edit: false,
  },
];

export default createStore({
  state: {
    listTasks: defaultListTasks,
  },
  getters: {
    listTasks: (state) => state.listTasks,
  },
  mutations: {
    addTasks(state, task) {
      state.listTasks.unshift(task);
    },

    removeTasks(state, id) {
      state.listTasks.splice(id, 1);
    },

    filterTasks(state, search) {
      if (search.length > 0) {
        state.listTasks = defaultListTasks.filter((taskFilter) => {
          return taskFilter.task.toLowerCase().includes(search.trim());
        });
      } else state.listTasks = defaultListTasks;
      return state.listTasks;
    },

    sortListTasks(state, selected) {
      console.log("selected: ", selected);
      if (selected === "Default") {
        return (state.listTasks = defaultListTasks.sort(
          (a, b) => new Date(a.date) - new Date(b.date)
        ));
      } else if (selected === "A - Z") {
        return (state.listTasks = defaultListTasks.sort((a, b) => {
          if (a.task.toUpperCase() < b.task.toUpperCase()) {
            return -1;
          } else if (a.task.toUpperCase() > b.task.toUpperCase()) {
            return 1;
          } else return 0;
        }));
      }
    },
  },
  actions: {},
  modules: {},
});
