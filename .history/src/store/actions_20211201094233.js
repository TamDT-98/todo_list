import axios from "axios";

const baseURL = "https://61a0b7a86c3b400017e69a00.mockapi.io/defaultListTasks";

export const getListTasks = ({ commit }) => {
  axios
    .get("https://61a0b7a86c3b400017e69a00.mockapi.io/defaultListTasks")
    .then((response) => {
      commit("GET_LIST_TASKS", response.data);
    });
};

export const removeTasks = () => {
  const confirmBox = confirm("Are you sure you want to remove this task?");
  if (confirmBox)
    axios
      .delete(`${baseURL}/${id}`)
      .then((result) => {
        console.warn(result);
        this.getListTasks();
        console.log("this.getListTasks(): ", this.getListTasks());
      })
      .catch((error) => {
        console.log(error);
      });
};
