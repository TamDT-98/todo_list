import { createStore } from "vuex";

import * as actions from "./actions";
import * as mutations from "./mutations";

export default createStore({
  state: {
    listTasks: [],
  },
  getters: {
    listTasks: (state) => state.listTasks,
  },
  mutations,
  actions,
  modules: {},
});
