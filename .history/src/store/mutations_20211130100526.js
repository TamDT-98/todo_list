export const SET_LISTTASKS = (state, listTasks) => {
  state.listTasks = listTasks;
};

export const addTasks = (state, task) => {
    state.listTasks.unshift(task);
  },

  export const removeTasks = (state, id) => {
    state.listTasks.splice(id, 1);
  },

  export const filterTasks = (state, search) => {
    if (search.length > 0) {
      state.listTasks = defaultListTasks.filter((taskFilter) => {
        return taskFilter.task.toLowerCase().includes(search.trim());
      });
    } else state.listTasks = defaultListTasks;
    return state.listTasks;
  },

  export const sortListTasks = (state, selected) => {
    if (selected === "Default") {
      return (state.listTasks = defaultListTasks.sort(
        (a, b) => new Date(a.date) - new Date(b.date)
      ));
    } else if (selected === "A - Z") {
      return (state.listTasks = defaultListTasks.sort((a, b) => {
        if (a.task.toUpperCase() < b.task.toUpperCase()) {
          return -1;
        } else if (a.task.toUpperCase() > b.task.toUpperCase()) {
          return 1;
        } else return 0;
      }));
    } else if (selected === "Z - A") {
      return (state.listTasks = defaultListTasks.sort((a, b) => {
        if (a.task.toUpperCase() < b.task.toUpperCase()) {
          return 1;
        } else if (a.task.toUpperCase() > b.task.toUpperCase()) {
          return -1;
        } else return 0;
      }));
    } else if (selected === "Completed") {
      return (state.listTasks = defaultListTasks.filter((completedFilter) => {
        return completedFilter.completed === true;
      }));
    } else if (selected === "Unfinished") {
      return (state.listTasks = defaultListTasks.filter((completedFilter) => {
        return completedFilter.completed === false;
      }));
    }
  },
