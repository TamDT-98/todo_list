import { createStore } from "vuex";

const defaultListTasks = [
  {
    task: "Learn VueJS",
    date: "17/12/2021",
    time: "11:58",
    completed: false,
    edit: false,
  },
  {
    task: "Learn React",
    date: "17/11/2021",
    time: "11:58",
    completed: false,
    edit: false,
  },
  {
    task: "Learn Javascript",
    date: "17/14/2021",
    time: "11:58",
    completed: false,
    edit: false,
  },
  {
    task: "Do something with VueJS",
    date: "17/22/2021",
    time: "11:58",
    completed: false,
    edit: false,
  },
  {
    task: "Do something with ReactJS",
    date: "17/16/2021",
    time: "11:58",
    completed: false,
    edit: false,
  },
  {
    task: "Do something with JavaScript",
    date: "17/13/2021",
    time: "11:58",
    completed: false,
    edit: false,
  },
];

export default createStore({
  state: {
    listTasks: defaultListTasks,
  },
  getters: {
    listTasks: (state) => state.listTasks,
  },
  mutations: {
    addTasks(state, task) {
      state.listTasks.push(task);
    },
    removeTasks(state, id) {
      state.listTasks.splice(id, 1);
    },
    filterTasks(state, search) {
      if (search.length > 0) {
        state.listTasks = defaultListTasks.filter((taskFilter) => {
          return taskFilter.task.toLowerCase().includes(search.trim());
        });
      } else state.listTasks = defaultListTasks;
      return state.listTasks;
    },
    // sortListTasks(state, status) {
    //   if (status === "Default") {
    //     state.listTasks = defaultListTasks.sort(
    //       (a, b) => new Date(a) - new Date(b)
    //     );
    //   }
    // },
  },
  actions: {},
  modules: {},
});
