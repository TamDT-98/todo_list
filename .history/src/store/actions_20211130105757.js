import axios from "axios";

export const getListTasks = ({ commit }) => {
  console.log("this.defaultListTasks: ");

  axios.get("http://localhost:3000/defaultListTasks").then((response) => {
    commit("GET_LIST_TASKS", response.data);
  });
};
