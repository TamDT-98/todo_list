import axios from "axios";

const baseURL = "http://localhost:3000/defaultListTasks";

export const getListTasks = ({ commit }) => {
  axios
    .get(baseURL)
    .then((response) => {
      commit("SET_LISTTASKS", response.date);
    })
    .catch((error) => {
      console.log(error);
    });
};
