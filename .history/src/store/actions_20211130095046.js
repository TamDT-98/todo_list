import axios from "axios";

const baseURL = "http://localhost:3000/defaultListTasks";

export default getListTasks = ({ commit }) => {
  axios
    .get(baseURL)
    .then((response) => {
      this.defaultListTasks = response.data.api;
    })
    .catch((error) => {
      console.log(error);
    });
};
