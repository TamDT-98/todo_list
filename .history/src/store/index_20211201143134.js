import { createStore } from "vuex";

import * as actions from "./actions";

export default createStore({
  state: {
    listTasks: [],
  },
  getters: {
    listTasks: (state) => state.listTasks,
  },
  mutations: {
    filterTasks() {
      if (this.search) {
        this.$router.push("/projects");
        this.listTasks.filter((taskFilter) => {
          taskFilter.task
            .toLowerCase()
            .includes(this.search.trim().toLowerCase());
          console.log("this.search: ", this.search);
          console.log("taskFilter: ", taskFilter);
        });
      }
    },

    sortListTasks(state, selected) {
      if (selected === "Default") {
        return state.listTasks.sort(
          (a, b) => new Date(b.date) - new Date(a.date)
        );
      } else if (selected === "A - Z") {
        return state.listTasks.sort((a, b) => {
          if (a.task.toUpperCase() < b.task.toUpperCase()) {
            return -1;
          } else if (a.task.toUpperCase() > b.task.toUpperCase()) {
            return 1;
          } else return 0;
        });
      } else if (selected === "Z - A") {
        return state.listTasks.sort((a, b) => {
          if (a.task.toUpperCase() < b.task.toUpperCase()) {
            return 1;
          } else if (a.task.toUpperCase() > b.task.toUpperCase()) {
            return -1;
          } else return 0;
        });
      } else if (selected === "Completed") {
        return state.listTasks.filter((completedFilter) => {
          return completedFilter.completed === true;
        });
      } else if (selected === "Unfinished") {
        return state.listTasks.filter((completedFilter) => {
          return completedFilter.completed === false;
        });
      }
    },

    ADD_TASK(state, tasks) {
      state.listTasks.unshift(tasks);
    },

    GET_LIST_TASKS(state, getListTasks) {
      state.listTasks = getListTasks.sort(
        (a, b) => new Date(b.date) - new Date(a.date)
      );
    },
  },
  actions,
  modules: {},
});
