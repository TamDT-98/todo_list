import { createStore } from "vuex";

export default createStore({
  state: {
    listTasks: [
      {
        task: "Learn VueJS",
        date: "17/11/2021",
        time: "11:58",
        completed: false,
        edit: false,
      },
      {
        task: "Learn React",
        date: "17/11/2021",
        time: "11:58",
        completed: false,
        edit: false,
      },
      {
        task: "Learn Javascript",
        date: "17/11/2021",
        time: "11:58",
        completed: false,
        edit: false,
      },
      {
        task: "Do something with VueJS",
        date: "17/11/2021",
        time: "11:58",
        completed: false,
        edit: false,
      },
    ],
  },
  getters: {
    listTasks: (state) => state.listTasks,
  },
  mutations: {
    addTasks(state, task) {
      state.listTasks.push(task);
    },
    removeTasks(state, id) {
      state.listTasks.splice(id, 1);
    },
    filterTasks(state, search) {
      return state.listTasks.filter((task) => {
        return task.task.match(search);
      });
    },
  },
  actions: {},
  modules: {},
});
