import { createStore } from "vuex";
import actions from "./actions";
import mutations from "./mutations";

const defaultListTasks = [];

export default createStore({
  state: {
    listTasks: defaultListTasks,
  },
  getters: {
    listTasks: (state) => state.listTasks,
  },
  mutations,
  actions,
  modules: {},
});
