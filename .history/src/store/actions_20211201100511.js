import axios from "axios";

const baseURL = "https://61a0b7a86c3b400017e69a00.mockapi.io/defaultListTasks";

export const getListTasks = ({ commit }) => {
  axios.get(baseURL).then((response) => {
    commit("GET_LIST_TASKS", response.data);
  });
};

export const removeTasks = (id) => {
  console.log("id: ", id);
  const confirmBox = confirm("Are you sure you want to remove this task?");
  if (confirmBox)
    axios
      .delete(`${baseURL}/${id}`)
      .then((result) => {
        console.warn(result);
      })
      .catch((error) => {
        console.log(error);
      });
};

export const addTasks = ({ commit }) => {
  axios.post(baseURL).then((response) => {
    commit("ADD_TASK"), response.data;
  });
};
