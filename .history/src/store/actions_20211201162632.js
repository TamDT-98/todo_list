import axios from "axios";

const baseURL = "https://61a0b7a86c3b400017e69a00.mockapi.io/defaultListTasks";

export const getListTasks = ({ commit }) => {
  axios.get(baseURL).then((response) => {
    commit("GET_LIST_TASKS", response.data);
  });
};

export const addTasks = ({ commit }, task) => {
  axios.post(baseURL, task).then((response) => {
    commit("ADD_TASK", response.data);
  });
};

export const updateTask = ({ commit }, id) => {
  console.log("id: ", id);
  axios.put(`${baseURL}/${id}`).then((response) => {
    commit("UPDATE_TASK", response.data);
  });
};

export const removeTasks = ({ commit }, id) => {
  console.log("id: ", id);
  const confirmBox = confirm("Are you sure you want to remove this task?");
  if (confirmBox)
    axios.delete(`${baseURL}/${id}`).then((response) => {
      commit("REMOVE_TASK", response.data);
    });
};
