import { createStore } from "vuex";

export default createStore({
  state: {
    listTasks: [
      {
        task: "Learn VueJS",
        date: "17/11/2021",
        time: "11:58",
        completed: false,
        edit: false,
      },
      {
        task: "Learn React",
        date: "17/11/2021",
        time: "11:58",
        completed: false,
        edit: false,
      },
      {
        task: "Learn Javascript",
        date: "17/11/2021",
        time: "11:58",
        completed: false,
        edit: false,
      },
      {
        task: "Do something with VueJS",
        date: "17/11/2021",
        time: "11:58",
        completed: false,
        edit: false,
      },
    ],
  },
  getters: {
    listTasks: (state) => state.listTasks,
  },
  mutations: {
    addTasks(state, task) {
      state.listTasks.push(task);
    },
    removeTasks(state, id) {
      state.listTasks.splice(id, 1);
    },
    filterTasks(state, search) {
      const newTaskLisk = state.listTasks;
      console.log(search.length);
      if (search.length > 0) {
        return (state.listTasks = state.listTasks.filter((taskFilter) => {
          return taskFilter.task.toLowerCase().includes(search);
        }));
      } else {
        return (state.listTasks = newTaskLisk);
      }
    },
  },
  actions: {},
  modules: {},
});
