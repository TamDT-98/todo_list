import { createStore } from "vuex";

export default createStore({
  state: {
    listTasks: [
      {
        task: "Learn VueJS",
        date: "17/11/2021",
        time: "11:58",
        completed: false,
      },
      {
        task: "Learn React",
        date: "17/11/2021",
        time: "11:58",
        completed: false,
      },
      {
        task: "Learn Javascript",
        date: "17/11/2021",
        time: "11:58",
        completed: false,
      },
      {
        task: "Do something with VueJS",
        date: "17/11/2021",
        time: "11:58",
        completed: false,
      },
    ],
  },
  getters: {
    listTasks: (state) => state.listTasks,
  },
  mutations: {
    addTasks(state, task) {
      state.listTasks.push(task);
    },
    removeTasks(state, task) {
      state.listTasks.splice(task, 1);
    },
  },
  actions: {},
  modules: {},
});
