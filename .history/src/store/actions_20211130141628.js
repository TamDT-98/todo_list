import axios from "axios";

export const getListTasks = ({ commit }) => {
  axios
    .get("https://61a0b7a86c3b400017e69a00.mockapi.io/defaultListTasks")
    .then((response) => {
      commit("GET_LIST_TASKS", response.data);
    });
};
