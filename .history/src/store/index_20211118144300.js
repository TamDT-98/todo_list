import { createStore } from "vuex";

export default createStore({
  state: {
    listTasks: [],
  },
  getters: {
    listTasks: (state) => state.listTasks,
  },
  mutations: {},
  actions: {},
  modules: {},
});
