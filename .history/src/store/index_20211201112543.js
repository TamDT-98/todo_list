import { createStore } from "vuex";

import * as actions from "./actions";

export default createStore({
  state: {
    defaultListTasks: [],
    listTasks: [],
  },
  getters: {
    listTasks: (state) => state.listTasks,
  },
  mutations: {
    filterTasks(state, search) {
      if (search.length > 0) {
        state.listTasks = state.defaultListTasks.filter((taskFilter) => {
          return taskFilter.task.toLowerCase().includes(search.trim());
        });
      } else state.listTasks = state.defaultListTasks;
      return state.listTasks;
    },

    sortListTasks(state, selected) {
      if (selected === "Default") {
        return state.defaultListTasks.sort(
          (a, b) => new Date(b.date) - new Date(a.date)
        );
      } else if (selected === "A - Z") {
        return state.defaultListTasks.sort((a, b) => {
          if (a.task.toUpperCase() < b.task.toUpperCase()) {
            return -1;
          } else if (a.task.toUpperCase() > b.task.toUpperCase()) {
            return 1;
          } else return 0;
        });
      } else if (selected === "Z - A") {
        return state.defaultListTasks.sort((a, b) => {
          if (a.task.toUpperCase() < b.task.toUpperCase()) {
            return 1;
          } else if (a.task.toUpperCase() > b.task.toUpperCase()) {
            return -1;
          } else return 0;
        });
      } else if (selected === "Completed") {
        return state.defaultListTasks.filter((completedFilter) => {
          return completedFilter.completed === true;
        });
      } else if (selected === "Unfinished") {
        return state.defaultListTasks.filter((completedFilter) => {
          return completedFilter.completed === false;
        });
      }
    },

    ADD_TASK(state, tasks) {
      console.log("tasks: ", tasks);
      state.defaultListTasks.unshift(tasks);
    },

    GET_LIST_TASKS(state, getListTasks) {
      state.defaultListTasks = getListTasks.sort(
        (a, b) => new Date(b.date) - new Date(a.date)
      );
      state.listTasks = state.defaultListTasks;
    },
  },
  actions,
  modules: {},
});
