import { createStore } from "vuex";

const defaultListTasks = [
  {
    task: "Learn VueJS",
    date: "17/11/2021",
    time: "11:58",
    completed: false,
    edit: false,
  },
  {
    task: "Learn React",
    date: "17/11/2021",
    time: "11:58",
    completed: false,
    edit: false,
  },
  {
    task: "Learn Javascript",
    date: "17/11/2021",
    time: "11:58",
    completed: false,
    edit: false,
  },
  {
    task: "Do something with VueJS",
    date: "17/11/2021",
    time: "11:58",
    completed: false,
    edit: false,
  },
  {
    task: "Do something with ReactJS",
    date: "17/11/2021",
    time: "11:58",
    completed: false,
    edit: false,
  },
  {
    task: "Do something with JavaScript",
    date: "17/11/2021",
    time: "11:58",
    completed: false,
    edit: false,
  },
];

export default createStore({
  state: {
    listTasks: [],
  },
  getters: {
    listTasks: (state) => state.listTasks.slice(0, 4),
  },
  mutations: {
    addTasks(state, task) {
      state.listTasks.push(task);
    },
    removeTasks(state, id) {
      state.listTasks.splice(id, 1);
    },
    filterTasks(state, search) {
      if (search.length > 0) {
        this.listFilter = state.listTasks.filter((taskFilter) => {
          return taskFilter.task.toLowerCase().includes(search.trim());
        });
      } else this.listFilter = state.listTasks;
      console.log("this.listFilter: ", this.listFilter);
      return this.listFilter;
    },
  },
  actions: {},
  modules: {},
});
