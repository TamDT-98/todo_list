import { createRouter, createWebHistory } from "vue-router";
import Home from "../views/Home.vue";

const routes = [
  {
    path: "/projects",
    name: "Projects",
    component: () => import("../views/Projects.vue"),
  },
  {
    path: "/addtask",
    name: "AddTask",
    component: () => import("../views/AddTask.vue"),
  },
  {
    path: "/stats",
    name: "Stats",
    component: () => import("../views/Stats.vue"),
  },
  {
    path: "/calender",
    name: "Calender",
    component: () => import("../views/Calender.vue"),
  },
  {
    path: "/",
    name: "Home",
    component: Home,
  },
];

router.replace({ path: "*", redirect: "/" });

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
