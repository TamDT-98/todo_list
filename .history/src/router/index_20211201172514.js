import { createRouter, createWebHistory } from "vue-router";
import Home from "../views/Home.vue";
import Projects from "../views/Projects.vue";
import AddTask from "../views/AddTask.vue";
import Stats from "../views/Stats.vue";
import Calendar from "../views/Calendar.vue";

const routes = [
  {
    path: "/projects",
    name: "Projects",
    component: Projects,
  },
  {
    path: "/addtask",
    name: "AddTask",
    component: AddTask,
  },
  {
    path: "/stats",
    name: "Stats",
    component: Stats,
  },
  {
    path: "/calendar",
    name: "Calendar",
    component: Calendar,
  },
  {
    path: "/",
    name: "Home",
    component: Home,
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
