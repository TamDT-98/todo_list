import { createRouter, createWebHistory } from "vue-router";
import Home from "../views/Home.vue";
import Projects from "../views/Projects.vue";

const routes = [
  {
    path: "/projects",
    name: "Projects",
    component: Projects,
  },
  {
    path: "/addtask",
    name: "AddTask",
    component: () => import("../views/AddTask.vue"),
  },
  {
    path: "/stats",
    name: "Stats",
    component: () => import("../views/Stats.vue"),
  },
  {
    path: "/calendar",
    name: "Calendar",
    component: () => import("../views/Calendar.vue"),
  },
  {
    path: "/",
    name: "Home",
    component: Home,
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
